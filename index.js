/**
 * Local Imports
 */
require('dotenv').config()

const config = require('./config/config').config
const mongo = require('./app/mongo');
const utils = require('./app/utils');
const telegram = require('./app/telegram');
const express  = require('express');

/**
 * Populates all our data objects eg. users, ledger, strings.
 */
function loadCollections(){
    Promise.all([
        mongo.loadCollection('users'),
    ])
    .then((res)=>{
        config.users=res[0]
    })
    .catch((err)=>{
        console.log(err)
    })
}


/**
 * Runs all the functions necessary to start the bot.
 */
function startBot(){
    utils.winston.info("STARTING")
    let app = express();
    app.listen(config.EXPRESS_PORT, () => utils.winston.info(`EXPRESS LISTENING ON PORT ${config.EXPRESS_PORT}`));
    loadCollections()
    telegram.bot.launch();
}


/**
 * Start everything!
 */
startBot();
